import { useState, useEffect } from "react"
import Cookies from "js-cookie"
import { useHistory } from "react-router-dom"
import axios from "axios"

const Login = () => {
    let history = useHistory();
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [login, setLogin] = useState(false)
    const [status, setStatus] = useState(false)
    const mail = "admin"
    const pass = "pass"
    const expireTime = new Date(new Date().getTime() + 120 * 60 * 1000);

    useEffect(() => {
      isLogin()
    }, [])
  
    const handleLogin = (e) => {
      e.preventDefault()
        if (email === mail && password === pass) {
          console.log("BERHASIL");
          localStorage.setItem("EMAIL_USER", email)
          Cookies.set("TOKEN_USER", "gerry", { expires: expireTime })
          setStatus(true)
          history.push("/admin");
        } else {
            setStatus(false)
        }
    }

      // const handleLogin = (e) => {
      //   e.preventDefault()
      //   const url = `https://jalin-app-backend.herokuapp.com/api/login`
      //   const data = { email, password }
    
      //   axios.post(url, data)
      //     .then(res => {
      //       console.log(res);
      //     })
      //     .catch(err => console.log(err))
      // }
  
    const isLogin = () => {
      if(typeof Cookies.get("TOKEN_USER") !== "undefined") {
        setLogin(true)
        return true;
      } else {
        setLogin(false)
        return false;
      }
    }
  
    return (
        <div>
            <form onSubmit={handleLogin}>
            <input type="text" onChange={(e) => setEmail(e.target.value)}></input><br />
            <input type="password" onChange={(e) => setPassword(e.target.value)}></input><br />
            <button type="submit">Login!</button>
            </form>
            {!status && "Password Salah" }
        </div>
    )
}

export default Login
