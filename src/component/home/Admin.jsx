import Headerdash from "./header/Headerdash";
import Footerdash from "./footer/Footerdash";
import Isi from "./dashboard/Isi"
import { Route, Switch, useHistory, useRouteMatch } from "react-router-dom"
import Cookies from "js-cookie";

const Admin = () => {
    let history = useHistory();
    const match = useRouteMatch();
    const handleLogout = () => {
        Cookies.remove("TOKEN_USER");
        localStorage.clear()
        history.push("/login");
      }
    return (
        <div>
            <h1>Selamat datang {Cookies.get("TOKEN_USER")}</h1>
            <button onClick={handleLogout}>Logout</button> 
            <Headerdash />
            <Switch>
                <Route exact path={`admin/table`}>
                <Isi />
                </Route>
                <Route exact path={`${match.path}/chart`}>
                <Isi />
                </Route>
                <Route exact path={`${match.path}/user`}>
                <Isi />
                </Route>
            </Switch>

            <Footerdash />  
            
        </div>
    )
}

export default Admin
