import { NavLink, useRouteMatch } from "react-router-dom"

const Headerdash = () => {
    const match = useRouteMatch();
    return (
        <div>
            <h1>Dashboard admin</h1>
            <NavLink className="m-5" exact to={`${match.url}/table`} activeClassName="active-link">table</NavLink>
            <NavLink className="m-5" to={`${match.url}/chart`} activeClassName="active-link">chart</NavLink>
            <NavLink className="m-5" to={`${match.url}/user`} activeClassName="active-link">user</NavLink>
            <hr />
        </div>
    )
}

export default Headerdash
