import Content from "./content/Content"
import Footer from "./footer/Footer"
import Header from "./header/Header"
import { Switch, Route } from "react-router-dom"
import About from "./content/About"
import Promo from "./content/Promo"
import Isipromo from "./content/Isipromo"

const Home = () => {
    return (
        <div>
            <Header />

            <Switch>
                <Route exact path="/promo">
                    <Promo />
                </Route>
                <Route exact path="/promo/detail/123">
                    <Isipromo />
                </Route>
                <Route exact path="/about">
                    <About />
                </Route>
                <Route path="/">
                    <Content />
                </Route>
            </Switch>
            
            <Footer />
        </div>
    )
}

export default Home
