import { NavLink } from "react-router-dom"
import "./class.css"
const Header = () => {
    return (
        <div>
            <h1>Ini Header</h1>
            <NavLink className="m-5" exact to="/" activeClassName="active-link">Home</NavLink>
            <NavLink className="m-5" to="/promo" activeClassName="active-link">Promo</NavLink>
            <NavLink className="m-5" to="/about" activeClassName="active-link">About</NavLink>
            <hr />
        </div>
    )
}

export default Header
