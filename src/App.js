import { Container } from "reactstrap";
import Home from "./component/landing-page/Home";
import { Switch, Route, Redirect } from "react-router-dom"
import Login from "./component/home/Login";
import Admin from "./component/home/Admin";

function App() {
  return (
    <div className="App">
      <Container>
      <h1>aplikasi</h1>
      {/* <Login /> */}
      <Switch>
      
      <Route exact path="/login">
          <Login/>
      </Route>
      <Route exact path="/">
          <Home />
      </Route>
      <Route path="/admin">
          <Admin />
      </Route>
      </Switch>
      </Container>
    </div>
  );
}

export default App;
